package model.data_structures;

import java.util.Iterator;

// C�digo obtenido de Diapositivas de Clase 4. Estructuras de Datos (ISIS -1206). Universidad de los Andes
// C�digo modificado por Laura Valentina Prieto Jim�nez 

public class LinkedList<E> implements ILinkedList<E>{
	
	private Node<E> listStartNode;
	private int listSize=0;
	
// La lista puede comenzar vacia	
	public LinkedList(){
		listStartNode=null;
	}
	
// La lista puede comenzar con alg�n elemento
	public LinkedList(E item){
		listStartNode=new Node<E>(item);
		listSize=1;
	}
	
	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<E>(){
			Node<E> actual=null;
			public boolean hasNext(){
				if(listSize==0){
					return false;
				}
				if(actual ==null){
					return true;
				}
				return actual.getNext()!=null;
			}
			
			public E next(){
				if(actual==null){
					actual=listStartNode;
				}
				else{
					actual=actual.getNext();
				}
				return actual.getItem();
			}	
		};
	}

	@Override
	public void addFirst(E item) {
		// TODO Auto-generated method stub
		if(listSize==0){
		listStartNode= new Node<E>(item);
		listSize=1;
		}
		else{
			Node<E> newNode =new Node<E>(item);
			newNode.setNext(listStartNode);
			listStartNode=newNode;
			listSize ++;
		}
	}

	@Override
	public void addEnd(E item) {
		// TODO Auto-generated method stub
		if(listSize==0){
			listStartNode=new Node<E>(item);
			listSize =1;
		}
		else{
			Node<E> lastNode=listStartNode;
			while(lastNode.getNext()!=null){
				lastNode=lastNode.getNext();
			}
			lastNode.setNext(new Node<E>(item));
			listSize++;
		}	
	}

	//Se asume que las posiciones que recibe empiezan desde cero(0)
	@Override
	public void remove(int p) {
		// TODO Auto-generated method stub
		if(p<0||p>(listSize-1)){
			throw new ArrayIndexOutOfBoundsException("La posici�n insertada "+p+" no se encuentra en la lista");
		}
		if(p==0){
			Node<E> nextNode=listStartNode.getNext();
			listStartNode.setNext(null);
			listStartNode=null;
			listStartNode=nextNode;
			listSize --;
		}
		else{
			Node<E> beforeNode=listStartNode;
			for(int i=1;i<p;i++){
				beforeNode=beforeNode.getNext();
			}
			Node<E> removeNode=beforeNode.getNext();
			beforeNode.setNext(removeNode.getNext());
			removeNode.setNext(null);
			listSize --;
		}
		
	}

	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		return listSize;
	}


	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if(listSize==0){
			return true;
		}
		else{
			return false;
		}
	}
	
	//Se asume que las posiciones que recibe empiezan desde cero(0)
	@Override
	public E getCurrentElement(int p) {
		// TODO Auto-generated method stub
		if(p<0||p>(listSize-1)){
			throw new ArrayIndexOutOfBoundsException("La posici�n insertada "+p+" no se encuentra en la lista");
		}
		Node<E> currentNode=listStartNode;
		for(int i=1;i<=p;i++){
			currentNode=currentNode.getNext();
		}
		return currentNode.getItem();
	}

	//M�todo auxiliar para implementar las pruebas
	//Se asume que las posiciones que recibe empiezan desde cero(0)
		@Override
		public Node<E> getCurrentNode(int p) {
			// TODO Auto-generated method stub
			if(p<0||p>(listSize-1)){
				throw new ArrayIndexOutOfBoundsException("La posici�n insertada "+p+" no se encuentra en la lista");
			}
			Node<E> currentNode=listStartNode;
			for(int i=1;i<=p;i++){
				currentNode=currentNode.getNext();
			}
			return currentNode;
		}
}
