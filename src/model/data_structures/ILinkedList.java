package model.data_structures;

public interface ILinkedList<E> extends Iterable<E>
{
	// A�adir un elemento a la lista
	public void addFirst(E item);
	public void addEnd(E item);
	
	// Quitar un elemento de la lista
	public void remove(int p);
	
	//Consultar el tama�o
	public int getSize();
	
	//Recuperar un elemento de la lista en una posicion
	public E getCurrentElement(int p);
	
	//Informa si la lista esta vacia
	public boolean isEmpty();	
	
	//M�todo auxiliar para implementar las pruebas
	public Node<E> getCurrentNode(int p);

}
