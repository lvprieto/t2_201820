package model.data_structures;

//C�digo obtenido de Diapositivas de Clase 4. Estructuras de Datos (ISIS -1206). Universidad de los Andes
//C�digo modificado por Laura Valentina Prieto Jim�nez 

public class Node<E> {
	
	private Node<E> next;
	private E item;
	private Node<E>back;
	
	public Node(E item){
		next=null;
		back=null;
		this.item=item;
	}
	
	public Node<E> getNext() {
		return next;
	}
	
	public void setNext(Node<E> next) {
		this.next = next;
	}
	
	public E getItem() {
		return item;
	}
	
	public void setItem(E item) {
		this.item = item;
	}
	
	public Node<E> getBack() {
		return back;
	}
	
	public void setBack(Node<E> back) {
		this.back = back;
	}
	
	
	
}
