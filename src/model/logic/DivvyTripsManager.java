package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Iterator;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import sun.net.NetworkServer;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IDoublyLinkedList;

public class DivvyTripsManager implements IDivvyTripsManager {

	public DoublyLinkedList<VOStation> listStations;
	public DoublyLinkedList<VOTrip> listTrips;
	
	public DivvyTripsManager(){
		
		listStations=new DoublyLinkedList<VOStation>();
	
		listTrips=new DoublyLinkedList<VOTrip>();
	}

	//Codigo obtenido de https://gist.github.com/inazense/0b5134b65741b3e21bbd95b14e5ea401
	//Codigo modificado por Laura Valentina Prieto Jim�nez
	public void loadStations (String stationsFile) {
		// TODO Auto-generated method stub

		try{
			// Abrir el .csv en buffer de lectura
			BufferedReader bufferLectura = new BufferedReader(new FileReader(stationsFile));

			// Leer una l�nea del archivo
			String linea = bufferLectura.readLine();
			linea=bufferLectura.readLine();

			while (linea != null) {
				// Separar la l�nea le�da con el separador definido previamente
				String[] campos = linea.split(",");
				int pId=Integer.parseInt(campos[0]);
				String pName=campos[1];
				String pCity=campos[2];
				double pLatitude=Double.parseDouble(campos[3]);
				double pLongitude=Double.parseDouble(campos[4]);
				int pDpcapacity=Integer.parseInt(campos[5]);
				String pOnline_Date=campos[6];

				VOStation newStation= new VOStation(pId, pName, pCity, pLatitude, pLongitude, pDpcapacity, pOnline_Date);
				listStations.addEnd(newStation);

				// Leer la siguiente l�nea del archivo
				linea = bufferLectura.readLine();
			}

			bufferLectura.close();
		}
		catch(Exception e){
			System.out.print("Hubo una falla al cargar las estaciones");
		}

	}

	//Codigo obtenido de https://gist.github.com/inazense/0b5134b65741b3e21bbd95b14e5ea401
	//Codigo modificado por Laura Valentina Prieto Jim�nez
	public void loadTrips (String tripsFile) {
		// TODO Auto-generated method stub
		try{
			BufferedReader bufferLectura = new BufferedReader(new FileReader(tripsFile));
			
			String linea = bufferLectura.readLine();
			linea=bufferLectura.readLine();

			while (linea != null) {
				String[] info=linea.split(",");
				
				int pTripID=Integer.parseInt(info[0]);
				String pStartTime =info[1];
				String pEndTime =info[2];
				int pBikeID=Integer.parseInt(info[3]);
				double pTripDuration=Double.parseDouble(info[4]);
				int pFromStationID=Integer.parseInt(info[5]);
				String pFromStationName=info[6];
				int pToStationID=Integer.parseInt(info[7]);
				String pToStationName=info[8];
				String pUserType=info[9];
				String pGender=(info.length<=10)?"No specified":info[10];
				
				VOTrip trip=new VOTrip(pTripID, pStartTime, pEndTime, pBikeID, pTripDuration, pFromStationID, pFromStationName, pToStationID, pToStationName, pUserType, pGender);
				listTrips.addEnd(trip);	
				
				linea=bufferLectura.readLine();
			}
			
			bufferLectura.close();
		}
		catch(Exception e){
			System.out.println("Hubo una falla al cargar los viajes");
		}
	}

	@Override
	public IDoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		// TODO Auto-generated method stub
		DoublyLinkedList<VOTrip> answer=new DoublyLinkedList<>();
		Iterator<VOTrip> iterator= listTrips.iterator();
		while(iterator.hasNext()){
			VOTrip element=iterator.next();
			if(element.getGender().equals(gender)){
				answer.addEnd(element);
			}
		}
		return answer;
	}

	@Override
	public IDoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		// TODO Auto-generated method stub
		DoublyLinkedList<VOTrip> answer=new DoublyLinkedList<>();
		Iterator<VOTrip> iterator= listTrips.iterator();
		while(iterator.hasNext()){
			VOTrip element=iterator.next();
			if(element.getToStationId()==stationID){
				answer.addEnd(element);
			}
		}
		return answer;
	}	


}
