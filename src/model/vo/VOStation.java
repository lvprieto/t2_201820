package model.vo;

/**
 * Representation of a Station object
 */
public class VOStation {
	
	int id;
	String name;
	String city;
	double latitude;
	double longitude;
	int dpcapacity;
	String online_Date;
	
	public VOStation(int pId, String pName, String pCity, double pLatitude, double pLongitude, int pDpcapacity, String pOnline_Date)
	{
		id=pId;
		name=pName;
		city=pCity;
		latitude=pLatitude;
		longitude=pLongitude;
		dpcapacity=pDpcapacity;
		online_Date=pOnline_Date;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getCity() {
		return city;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public int getDpcapacity() {
		return dpcapacity;
	}

	public String getOnline_Date() {
		return online_Date;
	}


	
	
}
