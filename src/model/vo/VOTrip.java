package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip {

	int trip_id;
	String startTime;
	String endTime;
	int bikeId;
	double tripDuration;
	String fromStationName;
	String toStationName;
	int fromStationId;
	int toStationId;
	String userType;
	String gender;
	
	public VOTrip(int pTrip_id, String pStartTime, String pEndTime, int pBikeId, double pTripDuration, int pFromStationId,String pFromStationName , int pToStationId, String pToStationName, String pUserType, String pGender)
	{
		trip_id=pTrip_id;
		startTime=pStartTime;
		endTime=pEndTime;
		bikeId=pBikeId;
		tripDuration=pTripDuration;
		fromStationName=pFromStationName;
		toStationName=pToStationName;
		fromStationId=pFromStationId;
		toStationId=pToStationId;
		userType=pUserType;
		gender=pGender;
	}
	
	
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return trip_id;
	}	
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return station_id - Origin Station Id .
	 */
	public int getFromStationId() {
		// TODO Auto-generated method stub
		return fromStationId;
	}
	
	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		// TODO Auto-generated method stub
		return fromStationName;
	}
	
	/**
	 * @return station_id - Destination Station Id .
	 */
	public int getToStationId() {
		// TODO Auto-generated method stub
		return toStationId;
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		// TODO Auto-generated method stub
		return toStationName;
	}
	
	/**
	 * @return gender - Gender od the user .
	 */
	public String getGender() {
		// TODO Auto-generated method stub
		return gender;
	}
	
}
