package ListTest;

import junit.framework.TestCase;
import model.data_structures.DoublyLinkedList;
import model.data_structures.Node;
import java.util.Iterator;

import org.junit.Test;


public class DoublyLinkedListTest extends TestCase {

	/**
     * Clase donde se har�n las pruebas.
     */
	private DoublyLinkedList<Integer> list;
	
	/**
     * Escenario 1: Construye una nueva lista doblemente encadenada vac�a
     */
    private void setupEscenario1( )
    {
    	list=new DoublyLinkedList<Integer>(); 
    }
    
    /**
     * Escenario 2: Construye una nueva lista doblemente encadenada con un elemento
     */
    private void setupEscenario2( )
    {
    	list=new DoublyLinkedList<Integer>((Integer)3); 
    }
    

    /**
     * Prueba 1: Prueba el m�todo constructor de la clase DoublyLinkedList. <br>
     * <b>M�todos a probar:</b> <br>
     * DoublyLinkedList con un elemento<br>
     * getSize<br>
     * getCurrentElement<br>
     * <b> Casos de prueba: </b><br>
     * 1. La lista doblemente encadenada fue creada y el tama�o de la lista corresponde.
     * 2. La lista doblemente encadenada fue creada y el elemento de la lista corresponde.
     * 
     */
    public void testDoublyLinkedList( )
    {
        setupEscenario2( );
        assertEquals( "La lista debio haberse inicializado con tama�o de 1", 1,list.getSize());
        assertNotNull("La lista no debe inicializarse vacia", list.getCurrentElement(0));
        assertEquals( "La lista debio haberse inicializado con el elemento 3", (Integer)3, list.getCurrentElement(0));
    }
    
    /**
     * Prueba 2: Prueba el m�todo constructor vacio de la clase DoublyLinkedList. <br>
     * <b>M�todos a probar:</b> <br>
     * DoublyLinkedList vacio<br>
     * getCurrentElement
     * addFirst<br>
     * addEnd<br>
     * getSize<br>
     * iterator<br>
     * <b> Casos de prueba: </b><br>
     * 1. La lista doblemente encadenada fue creada y el elemento de la lista corresponde.
     * 2. Agregar un elemento al principio de la lista.
     * 3. Agregar un elemento al final de la lista. 
     * 4. Utilizar el metodo iterador para probar que se agregaron correctamente los elementos
     * 
     */
    public void testEmptyDoublyLinkedList( )
    {
    	setupEscenario1( );
    	Iterator<Integer> it=list.iterator();	
        assertEquals( "La lista debio haberse inicializado vacia y tener tama�o 0",0, list.getSize());
        assertFalse("La lista no debe tener siguiente para iterar", it.hasNext());
        Integer i1=12;
        Integer i2=5;
        list.addFirst(i1);
        assertEquals("La lista debe tener como primer elemento a 12",(Integer)12, list.getCurrentElement(0));
        list.addEnd(i2);
        assertEquals( "La lista debe tener como ultimo elemento a 5", (Integer)5, list.getCurrentElement(1));
        assertTrue("La lista debe tener siguiente para iterar", it.hasNext());
        assertEquals("El elemento que se espera tener en el primer siguiente es 12", (Integer)12, it.next());
        assertEquals("El elemento que se espera tener en el segundo siguiente es 5", (Integer)5, it.next());
    }
    
    /**
     * Prueba 3: Prueba el m�todo remove en la primera posici�n <br>
     * <b>M�todos a probar:</b> <br>
     * isEmpty<br>
     * remove<br>
     * <b> Casos de prueba: </b><br>
     * 1. Eliminar el unico elemento que tiene la lista, lo cual conlleva a que la lista est� vacia
     * 
     */
    public void testRemove1(){
    	setupEscenario2();
    	list.remove(0);
    	assertTrue("La lista deber�a estar vacia", list.isEmpty());
    }
    
    /**
     * Prueba 4: Prueba el m�todo remove en la �ltima posici�n <br>
     * <b>M�todos a probar:</b> <br>
     * addEnd<br>
     * remove<br>
     * getCurrentNode<br>
     * <b> Casos de prueba: </b><br>
     * 1. Eliminar el tercer y ultimo nodo de la lista
     * 
     */
    public void testRemove2(){
    	setupEscenario2();
    	list.addEnd((Integer)8);
    	list.addEnd((Integer)24);
    	list.remove(2);
    	assertEquals("Al eliminar el nodo 3, el tama�o de la lista deber�a ser 2",2,list.getSize() );
    	assertNull("El siguiente nodo de 2 deber�a ser null",list.getCurrentNode(1).getNext());
    }
    
    /**
     * Prueba 5: Prueba el m�todo remove en una posici�n diferente de la primera y la �ltima <br>
     * <b>M�todos a probar:</b> <br>
     * addEnd<br>
     * remove<br>
     * getCurrentElement<br>
     * getCurrentNode<br>
     * <b> Casos de prueba: </b><br>
     * 1. Eliminar el segundo nodo de la lista
     * 
     */
    public void testRemove3(){
    	setupEscenario2();
    	list.addEnd((Integer)8);
    	list.addEnd((Integer)24);
    	list.addEnd((Integer)41);
    	list.addEnd((Integer)18);
    	list.remove(1);
    	assertEquals("Al eliminar el nodo 2, el tama�o de la lista deber�a ser 4",4,list.getSize() );
    	assertEquals("El nodo 2 deber�a ser ahora el nodo 3",(Integer)24,list.getCurrentElement(1));
    	assertEquals("El siguiente nodo del nodo 1 deber�a ser el nodo 3", (Integer) 24, list.getCurrentNode(0).getNext().getItem());
    }
    
    /**
     * Prueba 6: Prueba el m�todo remove en la primera posici�n <br>
     * <b>M�todos a probar:</b> <br>
     * getSize<br>
     * remove<br>
     * <b> Casos de prueba: </b><br>
     * 1. Eliminar el unico elemento que tiene la lista, lo cual conlleva a que la lista est� vacia
     * 
     */
    public void testRemove4(){
    	setupEscenario2();
    	list.addEnd((Integer)11);
    	list.remove(0);
    	assertEquals("La lista deber�a tener tama�o 1",1, list.getSize());
    	assertNull("El nodo siguiente al nodo 1 deber�a ser null", list.getCurrentNode(0).getNext());
    	assertNull("El nodo anterior al nodo 1 deber�a ser null", list.getCurrentNode(0).getBack());
    	assertEquals("El nodo uno deber�a tener el elemento 11", (Integer)11, list.getCurrentElement(0));
    }
}
